using System;

namespace API.Entities
{
    public class Question
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime Deadline { get; set; }
        public Boolean Asked { get; set; }
    }
}