using System;

namespace API.Entities
{
    public class Messages
    {
        public int Id { get; set; }

        public int SenderId { get; set; }

        public User Sender { get; set; }

        public int RecipientId { get; set; }

        public User Receiver { get; set; }

        public string Content { get; set; }

        public DateTime TimeSent { get; set; }

        public Question Question { get; set; }
        public int QuestionId { get; set; }
        public bool graded { get; set; }
    }
}