﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class QuestionToMessageBond : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Messages_QuestionId",
                table: "Messages",
                column: "QuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Question_QuestionId",
                table: "Messages",
                column: "QuestionId",
                principalTable: "Question",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Question_QuestionId",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_QuestionId",
                table: "Messages");
        }
    }
}
