using System;

namespace API.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime Deadline { get; set; }
        public Boolean Asked { get; set; }
    }
}