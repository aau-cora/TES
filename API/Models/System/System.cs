using System.Collections.Generic;

namespace API.Models.System
{
    public class SystemModel
    {
        public int Id { get; set; }
        public bool showScoreboard { get; set; }
    }
}