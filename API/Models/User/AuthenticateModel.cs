using System.ComponentModel.DataAnnotations;

namespace API.Models.User
{
    public class AuthenticateModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Password { get; set; }
    }
}