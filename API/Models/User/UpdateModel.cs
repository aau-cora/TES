namespace API.Models.User
{
    public class UpdateModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public int Score { get; set; }
        public bool isAdmin { get; set; }
    }
}