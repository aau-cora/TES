using System.Collections.Generic;

namespace API.Models.User
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public bool isAdmin { get; set; }
    }
}