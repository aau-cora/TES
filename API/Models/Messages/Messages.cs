using System;
using API.Models.User;

namespace API.Models.Messages
{
    public class MessagesModel
    {
        public int Id { get; set; }

        public int SenderId { get; set; }

        public UserModel Sender { get; set; }

        public int RecipientId { get; set; }

        public UserModel Receiver { get; set; }

        public string Content { get; set; }

        public DateTime TimeSent { get; set; }
        public QuestionModel Question { get; set; }
        public int QuestionId { get; set; }
        public bool graded { get; set; }
    }
}