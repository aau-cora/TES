using System;
using API.Models.User;

namespace API.Models.Messages
{
    public class UpdateModel
    {
        public int id { get; set; }
        public bool graded { get; set; }
    }
}