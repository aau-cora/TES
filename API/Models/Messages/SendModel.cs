using System;
using System.ComponentModel.DataAnnotations;
using API.Models.User;

namespace API.Models.Messages
{
    public class SendModel
    {
        [Required]
        public int SenderId { get; set; }
        [Required]
        public int RecipientId { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public int QuestionId { get; set; }
    }
}