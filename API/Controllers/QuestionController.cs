using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using API.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using API.Services;
using API.Entities;
using AutoMapper;


namespace API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class QuestionController : ControllerBase
    {
        private IQuestionService _questionService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public QuestionController(
            IQuestionService questionService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _questionService = questionService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Create a new question
        /// </summary>
        /// <param name="model"></param>
        /// <returns>HttpCode 200 if success or HttpCode 400 for BadRequest with exception message</returns>
        [HttpPost("Create")]
        public IActionResult Create([FromBody] Question model)
        {
            // map model to entity
            var question = _mapper.Map<Question>(model);

            // Try to create the question.
            try
            {
                _questionService.Create(question);
                return Ok();
            }
            // return error message if there was an exception
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get All Questions in the system
        /// </summary>
        /// <returns>HttpCode 200 with a list of questions</returns>
        [HttpGet]
        public IActionResult GetAll()
        {
            var questions = _questionService.GetAll();
            var model = _mapper.Map<IList<Question>>(questions);
            return Ok(model);
        }

        /// <summary>
        /// Get information about a specific question based on its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>HttpCode 200 with the question model</returns>
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var question = _questionService.GetById(id);
            var model = _mapper.Map<Question>(question);
            return Ok(model);
        }

        /// <summary>
        /// Git info about if user has answered the question
        /// </summary>
        /// <param name="QuestionId"></param>
        /// <param name="UserId"></param>
        /// <returns>HttpCode 200 with boolean</returns>
        [HttpGet("{QuestionId}/{UserId}")]
        public IActionResult GradeAble(int QuestionId, int UserId)
        {
            var gradeable = _questionService.Gradable(QuestionId, UserId);
            return Ok(gradeable);
        }

        /// <summary>
        /// Get list if questions which a user has answered
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>HttpCode 200 with list of questions answered</returns>
        [HttpGet("/Answers/{UserId}")]
        public IActionResult Answers(int UserId)
        {
            var gradeable = _questionService.Answers(UserId);
            return Ok(gradeable);
        }

        /// <summary>
        /// Update an existing question with a new model.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns>HttpCode 200 for Success and HttpCode 400 for BadRequest</returns>
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Question model)
        {
            // Map the model to the entity and set the id within that model.
            var question = _mapper.Map<Question>(model);
            question.Id = id;
            // Try to update the question with the model passed from HttpPut
            try
            {
                _questionService.Update(question);
                return Ok();
            }
            // Catch the exception and return a HttpBadRequest with the same message.
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Delete a question based on its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>HttpCode 200 for success</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _questionService.Delete(id);
            return Ok();
        }
    }
}