using Microsoft.AspNetCore.Mvc;
using API.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using API.Services;
using API.Models.User;
using AutoMapper;
using API.Models.System;

namespace API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class SystemController : ControllerBase
    {
        private ISystemService _systemService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public SystemController(
            ISystemService systemService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _systemService = systemService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Get information about the system
        /// </summary>
        /// <param></param>
        /// <returns>HttpCode 200 with the system model</returns>
        [HttpGet()]
        public IActionResult GetSystem()
        {
            var system = _systemService.GetSystem();
            var model = _mapper.Map<SystemModel>(system);
            return Ok(model);
        }

        /// <summary>
        /// Update an existing system with a new model.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns>HttpCode 200 for Success and HttpCode 400 for BadRequest</returns>
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] API.Entities.System model)
        {
            // Map the model to the entity and set the id within that model.
            var system = _mapper.Map<API.Entities.System>(model);
            system.Id = id;
            // Try to update the system with the model passed from HttpPut
            try
            {
                _systemService.Update(system);
                return Ok();
            }
            // Catch the exception and return a HttpBadRequest with the same message.
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}