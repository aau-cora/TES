using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using API.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using API.Services;
using API.Entities;
using AutoMapper;
using API.Models.Messages;

namespace API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {
        private IMessageService _messageService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public MessageController(
            IMessageService messageService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _messageService = messageService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Create a new message
        /// </summary>
        /// <param name="model"></param>
        /// <returns>HttpCode 200 on success with the message returned or HttpCode 400 for BadRequest with exception message</returns>
        [HttpPost("Send")]
        public IActionResult Register([FromBody] SendModel model)
        {
            // map model to entity
            var Message = _mapper.Map<Messages>(model);

            // Try to create the message.
            try
            {
                var result = _messageService.Create(Message);
                result.Sender = null; // Removes sensitive user information
                result.Receiver = null;// Removes sensitive user information
                return Ok(Message);
            }
            // return error message if there was an exception
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get All Messages in the system
        /// </summary>
        /// <returns>HttpCode 200 with a list of messages</returns>
        [HttpGet]
        public IActionResult GetAll()
        {
            var messages = _messageService.GetAll();
            var model = _mapper.Map<IList<MessagesModel>>(messages);
            return Ok(model);
        }

        /// <summary>
        /// Get information about a specific message based on its sender Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>HttpCode 200 with the message model</returns>
        [HttpGet("Sender/{id}")]
        public IActionResult GetBySenderId(int id)
        {
            var message = _messageService.GetBySenderId(id);
            var model = _mapper.Map<IList<MessagesModel>>(message);
            return Ok(model);
        }

        /// <summary>
        /// Get information about a specific message based on its receiver Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>HttpCode 200 with the message model</returns>
        [HttpGet("Receiver/{id}")]
        public IActionResult GetByReceiverId(int id)
        {
            var message = _messageService.GetByReceiverId(id);
            var model = _mapper.Map<IList<MessagesModel>>(message);
            return Ok(model);
        }
        /// <summary>
        /// Update an existing message with a new model.
        /// Only the graded property is allowed to change.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns>HttpCode 200 for Success and HttpCode 400 for BadRequest</returns>
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UpdateModel model)
        {
            // Map the model to the entity and set the id within that model.
            var message = _mapper.Map<Messages>(model);
            message.Id = id;
            // Try to update the user with the model passed from HttpPut
            try
            {
                _messageService.Update(message);
                return Ok();
            }
            // Catch the exception and return a HttpBadRequest with the same message.
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}