using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using API.Entities;

namespace API.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // Configures the MySql connection string.
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
                    => optionsBuilder.UseMySQL(Configuration.GetConnectionString("DefaultConnection"));

        public DbSet<User> User { get; set; }
        public DbSet<Messages> Messages { get; set; }
        public DbSet<Question> Question { get; set; }
        public DbSet<API.Entities.System> System { get; set; }
    }
}