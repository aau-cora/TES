using AutoMapper;
using API.Entities;
using API.Models.User;
using API.Models.Messages;
using API.Models;
using API.Models.System;

namespace API.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // User mappings
            CreateMap<User, UserModel>();
            CreateMap<Models.User.RegisterModel, User>();
            CreateMap<Models.User.UpdateModel, User>();

            CreateMap<Messages, MessagesModel>();
            CreateMap<Models.Messages.SendModel, Messages>();
            CreateMap<Models.Messages.UpdateModel, Messages>();

            CreateMap<Question, QuestionModel>();
            CreateMap<Models.QuestionModel, Question>();

            CreateMap<API.Entities.System, SystemModel>();
        }
    }
}