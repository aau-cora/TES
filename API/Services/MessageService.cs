using System;
using System.Collections.Generic;
using System.Linq;
using API.Entities;
using API.Helpers;
using Microsoft.EntityFrameworkCore;

namespace API.Services
{
    public class MessageService : IMessageService
    {
        private DataContext _context;

        public MessageService(DataContext context)
        {
            _context = context;
        }

        public Messages Create(Messages message)
        {
            // Check if the message contains any content
            if (string.IsNullOrWhiteSpace(message.Content))
                throw new AppException("You need to enter a message");

            message.Receiver = _context.User.FirstOrDefault(x => x.Id == message.RecipientId);
            message.Sender = _context.User.FirstOrDefault(x => x.Id == message.SenderId);
            message.Question = _context.Question.FirstOrDefault(x => x.Id == message.QuestionId);
            message.TimeSent = DateTime.Now;
            if (message.Question == null)
                throw new AppException("The message must be related to a question!");

            // Check if sender exists
            if (message.Sender == null)
                throw new AppException("Sender does not exist");

            if (message.Receiver == null)
                throw new AppException("Receiver does not exist");
            
            // Check if the sender is not the admin and that the question has been asked.
            if (!message.Sender.isAdmin && message.Question.Asked == false)
                throw new AppException("Unknown question, please try answering another one");
        
            _context.Messages.Add(message);
            _context.SaveChanges();

            return message;
        }
        public IEnumerable<Messages> GetAll()
        {
            return _context.Messages.Include(i => i.Sender).Include(i => i.Receiver).Include(i => i.Question);
        }

        public IEnumerable<Messages> GetBySenderId(int id)
        {
            return _context.Messages.Include(i => i.Sender).Include(i => i.Receiver).Include(i => i.Question).Where(x => x.SenderId == id);
        }

        public IEnumerable<Messages> GetByReceiverId(int id)
        {
            return _context.Messages.Include(i => i.Receiver).Include(i => i.Sender).Include(i => i.Question).Where(x => x.RecipientId == id);
        }

        public void Update(Messages messageParam)
        {
            var message = _context.Messages.Find(messageParam.Id);

            if (message == null)
                throw new AppException("Message not found");

            message.graded = messageParam.graded;

            _context.Messages.Update(message);
            _context.SaveChanges();
        }
    }
}