using System;
using System.Collections.Generic;
using System.Linq;
using API.Entities;
using API.Helpers;
using Microsoft.EntityFrameworkCore;

namespace API.Services
{
    public class QuestionService : IQuestionService
    {
        private DataContext _context;

        public QuestionService(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<Question> Answers(int userid)
        {
            var Messages = _context.Messages.Where(m => m.SenderId == userid).Include(q => q.Question);
            var result = Messages.Select(q => q.Question).Distinct().ToList();
            return result;
        }

        public Question Create(Question questionParam)
        {
            var question = new Question { };
            // Check if the question contains any content
            if (string.IsNullOrWhiteSpace(questionParam.Content))
                throw new AppException("You need to enter a question");
            question.Content = questionParam.Content;


            if (questionParam.Deadline.Equals(null))
                throw new AppException("You need to enter a deadline");

            question.Deadline = questionParam.Deadline;
            Console.WriteLine(questionParam.Deadline);

            question.Asked = questionParam.Asked;

            _context.Question.Add(question);
            _context.SaveChanges();

            return question;
        }

        public void Delete(int id)
        {
            var question = _context.Question.Find(id);
            if (question != null)
            {
                _context.Question.Remove(question);
                _context.SaveChanges();
            }
        }

        public IEnumerable<Question> GetAll()
        {
            return _context.Question;
        }

        public Question GetById(int id)
        {
            return _context.Question.Find(id);
        }

        public Boolean Gradable(int questionId, int userId)
        {
            var messages = _context.Messages.Where(x => x.QuestionId == questionId).
            Where(x => x.SenderId == userId).
            Include(x => x.Question).
            Include(x => x.Sender);

            List<Question> result = new List<Question>();
            foreach (var message in messages)
            {
                if (result.All(x => x.Id != message.QuestionId))
                {
                    result.Add(message.Question);
                }
            }
            if (result.Count() == 0)
            {
                return false;
            }
            else { return true; }
        }

        public void Update(Question question)
        {
            var oldQuestion = _context.Question.Find(question.Id);
            if (oldQuestion != null)
            {
                if (question.Content != null)
                    oldQuestion.Content = question.Content;

                // if old question has not be asked, then update with whatever is passed
                if (!oldQuestion.Asked)
                    oldQuestion.Asked = question.Asked;

                oldQuestion.Deadline = question.Deadline;

                _context.Update(oldQuestion);
                _context.SaveChanges();
            }
        }
    }
}