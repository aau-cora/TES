using System;
using System.Collections.Generic;
using API.Entities;

namespace API.Services
{
    // Interface for the UserService
    public interface IQuestionService
    {
        Question GetById(int id);
        IEnumerable<Question> GetAll();
        Boolean Gradable(int questionId, int userId);
        Question Create(Question question);
        void Update(Question question);
        void Delete(int id);
        IEnumerable<Question> Answers(int userid);
    }
}