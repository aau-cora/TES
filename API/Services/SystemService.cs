using System;
using System.Collections.Generic;
using System.Linq;
using API.Entities;
using API.Helpers;

namespace API.Services
{
    // UserService used by the UserController
    public class SystemService : ISystemService
    {
        private DataContext _context;

        public SystemService(DataContext context)
        {
            _context = context;
        }

        public void Update(API.Entities.System systemParam)
        {
            var system = _context.System.Find(systemParam.Id);

            system.showScoreboard = systemParam.showScoreboard;

            _context.System.Update(system);
            _context.SaveChanges();
        }

        public Entities.System GetSystem()
        {
            return _context.System.FirstOrDefault();
        }
    }
}