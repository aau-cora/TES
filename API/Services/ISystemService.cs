using System.Collections.Generic;
using API.Entities;

namespace API.Services
{
    // Interface for the UserService
    public interface ISystemService
    {
        API.Entities.System GetSystem();
        void Update(API.Entities.System system);
    }
}