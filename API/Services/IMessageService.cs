using System.Collections.Generic;
using API.Entities;

namespace API.Services
{
    // Interface for the MessageService
    public interface IMessageService
    {
        IEnumerable<Messages> GetAll();
        IEnumerable<Messages> GetBySenderId(int id);
        IEnumerable<Messages> GetByReceiverId(int id);
        Messages Create(Messages message);
        void Update(Messages message);
    }
}