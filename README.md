# CORA Tabletop Exercise System
> Tabletop Exercise System enabling interactive exercises

Interactive self-hosted system for live tabletop exercises.

The live version of the system can be found on [https://cora.cybertraining.dk](https://cora.cybertraining.dk)

![](images/Cora.jpg)
![](images/Interreg.jpg)

## Release History

* Nothing to report yet

## Meta

Main developer: Thomas Lundsgaard Kammersgaard [thluha@es.aau.dk](mailto:thluha@es.aau.dk)\
GPG Signature: 6DC8C36813A57FEFD02E10A1A9A4B65D881B40D5

[https://coraproject.eu](https://coraproject.eu/)

## Contributing

1. Fork it (<https://gitlab.com/aau-cora/TES>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
