# Change to API dir
cd API
# Build the API in configuration mode
dotnet publish --configuration Release
# Copy new API build to the server
scp -r bin/Release/netcoreapp3.1/* ubuntu@130.226.98.24:/home/ubuntu/out/
# Restart API service on the server
ssh ubuntu@srv.cora.cybertraining.dk 'sudo service kestrel-api restart'
# Check status on the API service
ssh ubuntu@srv.cora.cybertraining.dk 'sudo service kestrel-api status'
# Change to the SPA dir
cd ../SPA
# Build the SPA in production mode
ng build --prod
# Delete the old SPA from the server
ssh ubuntu@cora.cybertraining.dk  'rm -rf /home/ubuntu/SPA'
# Copy SPA to the server
scp -r dist/SPA ubuntu@cora.cybertraining.dk:/home/ubuntu/
# Restart the webserver to force the new SPA to the users
ssh ubuntu@cora.cybertraining.dk 'sudo service apache2 restart'
