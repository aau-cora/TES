import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';
import { MessageService } from '../_services/message.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  model: any = {};
  @Output() cancelRegister = new EventEmitter();

  constructor(
    private authService: AuthService,
    private alertifyService: AlertifyService,
    private router: Router,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  register(): void {
    this.authService.register(this.model).subscribe(
      () => {
        this.alertifyService.success('Registration success');
      },
      (error) => {
        this.alertifyService.error(error);
      },
      () => {
        this.authService.login(this.model).subscribe(
          () => this.alertifyService.success('You have been logged in!'),
          (error) => {
            this.alertifyService.error(error);
          },
          () => {
            localStorage.setItem('name', this.model.name);
            if (this.authService.isAdmin()) {
              this.router.navigate(['/question']);
            } else {
              this.router.navigate(['/messages']);
            }
          }
        );
      }
    );
  }

  cancel(): void {
    this.cancelRegister.emit(false);
    this.alertifyService.message('Registration cancelled!');
  }
}
