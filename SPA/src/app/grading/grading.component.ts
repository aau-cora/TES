import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../_services/question.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-grading',
  templateUrl: './grading.component.html',
  styleUrls: ['./grading.component.css'],
})
export class GradingComponent implements OnInit {
  users: any[];
  result: any[] = [];

  constructor(
    private questionService: QuestionService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userService
      .getUsers()
      .toPromise()
      .then((users) => {
        this.users = users;
      })
      .then(() => {
        this.users.forEach((user) => {
          this.questionService
            .Answers(user.id)
            .toPromise()
            .then((next) => {
              this.result.push([user, next]); // Adds user with their answers to the results array
            });
        });
      });
  }
}
