import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AuthService } from './_services/auth.service';
import { ErrorInterceptorProvider } from './_services/error.interceptor';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { MessagesComponent } from './messages/messages.component';
import { appRoutes } from './routes';
import { EditUserComponent } from './editUser/editUser.component';
import { GradingComponent } from './grading/grading.component';
import { QuestionComponent } from './question/question.component';
import { EditQuestionComponent } from './editQuestion/editQuestion.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { GradeComponent } from './grade/grade.component';
import { FooterComponent } from './footer/footer.component';
import { SystemAdminComponent } from './systemAdmin/systemAdmin.component';

export function tokenGetter(): string {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ScoreboardComponent,
    HomeComponent,
    RegisterComponent,
    MessagesComponent,
    EditUserComponent,
    GradingComponent,
    QuestionComponent,
    EditQuestionComponent,
    GradeComponent,
    FooterComponent,
    SystemAdminComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' }),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:5000', 'srv.cora.cybertraining.dk'],
        disallowedRoutes: [
          'localhost:5000/User/Authenticate',
          'srv.cora.cybertraining.dk/User/Authenticate',
        ],
      },
    }),
  ],
  providers: [
    AuthService,
    ErrorInterceptorProvider,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
