import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';
import { observeOn } from 'rxjs/operators';
import { SystemService } from '../_services/system.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {
  model: any = {};
  greeting: string;

  constructor(
    public authService: AuthService,
    private alertify: AlertifyService,
    private router: Router,
    private userService: UserService,
    private systemService: SystemService
  ) {}

  ngOnInit(): void {
    this.timeAppropriateGreeting();
  }

  timeAppropriateGreeting(): void {
    const date = new Date();
    const time = date.getHours();
    if (0 <= time && time <= 5) {
      this.greeting = 'Good night';
    } else if (6 <= time && time < 10) {
      this.greeting = 'Good morning';
    } else if (10 <= time && time < 14) {
      this.greeting = 'Good day';
    } else if (14 <= time && time < 18) {
      this.greeting = 'Good afternoon';
    } else if (18 <= time && time < 23) {
      this.greeting = 'Good night';
    } else {
      this.greeting = 'Hello';
    }
  }

  login(): void {
    this.authService
      .login(this.model)
      .toPromise()
      .then(() => {
        this.model.username = '';
        this.model.password = '';
        this.alertify.success('Successfully logged in!');
        if (this.admin()) {
          this.router.navigate(['/question']);
        } else {
          this.router.navigate(['/messages']);
        }
      })
      .catch((error) => {
        this.alertify.error(error.message);
      });
  }

  loggedIn(): boolean {
    return this.authService.loggedIn();
  }

  admin(): boolean {
    return this.authService.isAdmin();
  }

  show_scoreboard() {
    return this.systemService.systemSettings.showScoreboard;
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('name');
    this.alertify.message('Logged out');
    this.router.navigate(['/home']);
    this.authService.isAdminProp = false;
    this.ngOnInit();
  }
}
