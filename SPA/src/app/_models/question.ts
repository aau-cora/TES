export interface Question {
    id: number;
    content: string;
    asked: boolean;
}
