export interface User {
    id: number;
    name: string;
    score: number;
    isAdmin: boolean;
}
