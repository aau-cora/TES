export interface Message {
    content: string;
    timeSent: Date;
    senderId: number;
    recipientId: number;
    questionId: number;
    graded: boolean;
}
