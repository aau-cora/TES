import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../_models/user';
import { AlertifyService } from '../_services/alertify.service';
import { MessageService } from '../_services/message.service';
import { QuestionService } from '../_services/question.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-grade',
  templateUrl: './grade.component.html',
  styleUrls: ['./grade.component.css'],
})
export class GradeComponent implements OnInit {
  questionId: any;
  userId: any;
  question: any = {};
  user: any = {};
  messagesRet: any[];
  message: any = {};
  model: number;
  toLate: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private questionService: QuestionService,
    private messageService: MessageService,
    private alertifyService: AlertifyService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.questionId = this.route.snapshot.paramMap.get('questionId');
    this.userId = this.route.snapshot.paramMap.get('userId');
    this.questionService.getQuestion(this.questionId).subscribe((next) => {
      this.question = next;
      this.userService.getUser(this.userId).subscribe((next) => {
        this.user = next;
        this.messageService
          .getMessagesBySender(this.userId)
          .subscribe((next) => {
            this.messagesRet = next;
            this.messagesRet.forEach((message) => {
              if (message.questionId == this.questionId) {
                this.message = message;
                this.toLate =
                  this.calcTime(new Date(this.message.timeSent)) >
                  this.calcTime(new Date(this.question.deadline));
              }
            });
          });
      });
    });
  }

  calcTime(time: Date): Number {
    return time.getSeconds() + time.getHours() * 60;
  }

  cancel(): void {
    this.router.navigate(['/grading']);
  }

  grade(): void {
    if (this.model < 0 || this.model > 10) {
      this.alertifyService.warning('The grade must be between 0-10');
    } else {
      this.user.score += this.model;
      this.userService.updateUser(this.userId, this.user).subscribe(
        () => {
          this.alertifyService.success('Grade has been successfully added!');
          this.message.graded = true;
          this.messageService
            .updateMessage(this.message.id, this.message)
            .subscribe();
        },
        (error) => {
          this.alertifyService.error(error);
        }
      );
    }
  }
}
