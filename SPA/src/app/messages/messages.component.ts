import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { MessageService } from '../_services/message.service';
import { AlertifyService } from '../_services/alertify.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Message } from '../_models/message';
import { zip } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
})
export class MessagesComponent implements OnInit, OnDestroy {
  @ViewChild('scroll') private myScrollContainer: ElementRef;
  model: any = {};
  messages: any = [];
  newMessages: any = [];
  userId: number;
  jwtHelper = new JwtHelperService();
  interval: any;

  constructor(
    private messageService: MessageService,
    private alertify: AlertifyService
  ) {}

  ngOnInit(): void {
    const token = this.jwtHelper.decodeToken(localStorage.getItem('token'));
    this.userId = Number(token.unique_name);
    this.compileChatHistory();
    this.interval = setInterval(() => {
      this.compileChatHistory();
      if (this.messages.length != this.newMessages.length) {
        this.messages = this.newMessages;
        if (this.messages[this.messages.length - 1].senderId == 1) {
          this.alertify.confirm(
            'Message',
            'A message has been received!',
            () => {
              this.scrollToBottom();
            },
            'Show'
          );
        }
      }
    }, 2000);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {
      this.scrollToBottom();
    }
  }

  sortBy(messages: Message[]): Message[] {
    return messages.sort((a, b) => {
      return (new Date(b.timeSent) as any) - (new Date(a.timeSent) as any);
    });
  }

  compileChatHistory() {
    zip(
      this.messageService.getMessagesBySender(1),
      this.messageService.getMessagesBySender(this.userId)
    )
      .pipe(map((x) => x[0].concat(x[1])))
      .subscribe((m) => {
        this.newMessages = this.sortBy(m).reverse();
      });
  }

  sendMessage(): void {
    const messageToSend: any = {};
    messageToSend.content = this.model.content;
    messageToSend.senderId = this.userId;
    messageToSend.recipientId = 1;
    messageToSend.questionId = this.model.questionId;
    this.messageService
      .sendMessage(messageToSend)
      .toPromise()
      .then((message) => {
        this.compileChatHistory();
        this.scrollToBottom();
        this.model.content = '';
        this.model.questionId = '';
      })
      .catch((error) => {
        this.alertify.error(error.message);
      });
  }
}
