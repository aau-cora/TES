import { Component, OnInit } from '@angular/core';
import { Question } from '../_models/question';
import { AlertifyService } from '../_services/alertify.service';
import { QuestionService } from '../_services/question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css'],
})
export class QuestionComponent implements OnInit {
  questions: Question[];
  model: any = {};
  Deadline: Date;
  constructor(
    public questionService: QuestionService,
    public alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.questionService.getAllQuestions().subscribe((next) => {
      this.questions = next;
    });
  }

  create(): void {
    this.Deadline = new Date('0001-01-01T' + this.model.Deadline + ':00Z');
    this.model.Deadline = this.Deadline;
    this.questionService.create(this.model).subscribe(
      () => {
        this.alertify.success('Created successfully!');
        this.model.Deadline = '';
        this.model.content = '';
        this.ngOnInit();
      },
      (error) => {
        this.alertify.error(error);
      },
      () => {}
    );
  }

  sendQuestion(id: number) {
    const messageToSend: any = {};
    this.questionService
      .getQuestion(id)
      .toPromise()
      .then((next) => {
        messageToSend.content = next.content;
        messageToSend.senderId = 1;
        messageToSend.recipientId = 1;
        messageToSend.questionId = id;
        next.asked = true;
        this.questionService
          .sendQuestion(messageToSend)
          .toPromise()
          .then(() => {
            this.alertify.success('Message sent!');
          })
          .then(() => {
            this.questionService
              .update(id, next)
              .toPromise()
              .catch((error) => {
                this.alertify.error(error.message);
              });
          })
          .catch((error) => {
            this.alertify.error(error.message);
          });
      })
      .finally(() => {
        this.ngOnInit();
      });
  }
}
