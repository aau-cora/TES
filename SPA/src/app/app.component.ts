import { Component, OnInit } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserService } from './_services/user.service';
import { User } from './_models/user';
import { SystemService } from './_services/system.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'TES Cora';
  jwtHelper = new JwtHelperService();
  decodedToken: any;

  constructor(private authService: AuthService, private userService: UserService, private systemService: SystemService) { }

  ngOnInit() {
    // Decode stored JWT token to get id, and call API with that id to get username.
    const token = localStorage.getItem('token');
    if (token) {
      this.decodedToken = this.jwtHelper.decodeToken(token);
      this.userService.getUser(Number(this.decodedToken.unique_name)).subscribe(user => {
        this.authService.username = user.name;
        this.authService.isAdminProp = user.isAdmin;
        this.systemService.getSystem().subscribe(system => {
          this.systemService.systemSettings = system;
        });
      });
    }
  }
}
