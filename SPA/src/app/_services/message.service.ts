import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Message } from '../_models/message';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getAllMessages(): Observable<any> {
    return this.http.get(this.baseUrl + 'Message');
  }

  getMessagesBySender(id: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Message/Sender/' + id);
  }

  sendMessage(message: Message): Observable<any> {
    return this.http.post(this.baseUrl + 'Message/Send', message);
  }

  updateMessage(id: number, message: Message): Observable<any> {
    return this.http.put(this.baseUrl + 'Message/' + id, message);
  }
}
