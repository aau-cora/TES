import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Question } from '../_models/question';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
  providedIn: 'root',
})
export class QuestionService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getAllQuestions(): Observable<any> {
    return this.http.get(this.baseUrl + 'Question');
  }

  getQuestion(id: number): Observable<any> {
    return this.http.get(this.baseUrl + 'Question/' + id);
  }

  create(model: any): Observable<any> {
    return this.http.post(this.baseUrl + 'Question/Create', model);
  }

  update(id: number, model: any): Observable<any> {
    return this.http.put(this.baseUrl + 'Question/' + id, model);
  }

  sendQuestion(message: Message): Observable<any> {
    return this.http.post(this.baseUrl + 'Message/Send', message);
  }

  sendWelcomeMessage() {
    let messageRet: Message;
    this.getQuestion(1).subscribe((message) => {
      messageRet = message;
    });
    return this.sendQuestion(messageRet);
  }

  hasQuestionBeenAnswered(questionId: number, userId: number) {
    return this.http.get(
      this.baseUrl + 'Question/' + questionId + '/' + userId
    );
  }

  Answers(userId: number) {
    return this.http.get(
      this.baseUrl + 'Answers/' + userId
    );
  }
}
