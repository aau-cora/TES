import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../_models/user';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  baseUrl = environment.apiUrl;
  jwtHelper = new JwtHelperService();
  username: string;
  isAdminProp: boolean = false;

  constructor(private http: HttpClient, private userService: UserService) {}

  login(model: any): Observable<void> {
    return this.http.post(this.baseUrl + 'User/Authenticate', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          if(user.id == 1)
            this.isAdminProp = true;
          localStorage.setItem('token', user.token);
          localStorage.setItem('name', user.username);
          this.username = user.username;
        }
      })
    );
  }

  register(model: any): Observable<object> {
    return this.http.post(this.baseUrl + 'User/Register', model);
  }

  update(id: number, model: any): Observable<object> {
    return this.http.put(this.baseUrl + 'User/' + id, model);
  }

  loggedIn(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  isAdmin(): boolean{
    return this.isAdminProp;
  }
}
