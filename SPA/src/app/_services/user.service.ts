import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + 'User');
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(this.baseUrl + 'User/' + id);
  }

  updateUser(id: number, model: User) {
    return this.http.put(this.baseUrl + 'User/' + id, model);
  }
}
