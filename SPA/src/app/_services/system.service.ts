import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SystemService {
  baseUrl = environment.apiUrl;
  systemSettings: any = {};

  constructor(private http: HttpClient) {}

  getSystem(): Observable<any> {
    return this.http.get<any[]>(this.baseUrl + 'System');
  }

  updateSystem(id: number, model: any): Observable<any> {
    return this.http.put(this.baseUrl + 'System/' + id, model);
  }
}
