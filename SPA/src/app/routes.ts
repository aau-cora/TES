import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { MessagesComponent } from './messages/messages.component';
import { RegisterComponent } from './register/register.component';
import { EditUserComponent } from './editUser/editUser.component';
import { GradingComponent } from './grading/grading.component';
import { AuthGuard } from './_guards/auth.guard';
import { QuestionComponent } from './question/question.component';
import { EditQuestionComponent } from './editQuestion/editQuestion.component';
import { GradeComponent } from './grade/grade.component';
import { AdminGuard } from './_guards/admin.guard';
import { SystemAdminComponent } from './systemAdmin/systemAdmin.component';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      { path: 'messages', component: MessagesComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'editUser', component: EditUserComponent },
      { path: 'scoreboard', component: ScoreboardComponent },
    ],
  },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AdminGuard],
    children: [
      { path: 'scoreboard', component: ScoreboardComponent },
      { path: 'grading', component: GradingComponent },
      { path: 'grade/:questionId/:userId', component: GradeComponent },
      { path: 'question', component: QuestionComponent },
      { path: 'editQuestion/:id', component: EditQuestionComponent },
      { path: 'systemAdmin', component: SystemAdminComponent },
    ],
  },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];
