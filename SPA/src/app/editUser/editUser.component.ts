import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-editUser',
  templateUrl: './editUser.component.html',
  styleUrls: ['./editUser.component.css'],
})
export class EditUserComponent implements OnInit {
  model: any = {};
  userId: number;
  jwtHelper = new JwtHelperService();
  constructor(
    private authService: AuthService,
    private alertifyService: AlertifyService,
    private userService: UserService
  ) {}

  ngOnInit() {
    const token = this.jwtHelper.decodeToken(localStorage.getItem('token'));
    this.userId = Number(token.unique_name);
    this.userService.getUser(this.userId).subscribe((user) => {
      this.model = user;
    });
  }

  update(): void {
    this.authService.update(this.userId, this.model).subscribe(
      () => {
        this.alertifyService.success('Updated successfully!');
      },
      (error) => {
        this.alertifyService.error(error);
      }
    );
  }
}
