import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { AlertifyService } from '../_services/alertify.service';
import { SystemService } from '../_services/system.service';

@Component({
  selector: 'app-systemAdmin',
  templateUrl: './systemAdmin.component.html',
  styleUrls: ['./systemAdmin.component.css'],
})
export class SystemAdminComponent implements OnInit {
  model: any = {};

  constructor(
    private systemService: SystemService,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.model = this.systemService.systemSettings;
  }

  update(model: any) {
    this.model.showScoreboard = model;
    this.systemService.updateSystem(this.model.id, this.model).subscribe(
      () => {
        this.alertify.success('System has been updated!');
      },
      (error) => {
        this.alertify.error(error);
      }
    );
  }
}
