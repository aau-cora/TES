import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from '../_models/question';
import { QuestionService } from '../_services/question.service';
import { switchMap } from 'rxjs/operators';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-editQuestion',
  templateUrl: './editQuestion.component.html',
  styleUrls: ['./editQuestion.component.css'],
})
export class EditQuestionComponent implements OnInit {
  model: any = {};
  question: any = {};
  id: any;
  Deadline: Date;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private questionService: QuestionService,
    private alertifyService: AlertifyService
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.questionService.getQuestion(this.id).subscribe((next) => {
      this.question = next;
    });
  }

  update() {
    this.Deadline = new Date('0001-01-01T' + this.model.deadline + ':00Z');
    this.model.deadline = this.Deadline;

    this.questionService.update(this.id, this.model).subscribe(
      () => {
        this.alertifyService.success('Question updated');
        this.router.navigate(['/question']);
      },
      (error) => {
        this.alertifyService.error(error);
      }
    );
  }

  cancel(): void {
    this.router.navigate(['/question']);
  }
}
